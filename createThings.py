import os
import boto3
import json
import random
import string

################################################### Parameters for Thing
thingGroupName = 'thing-group'
thingGroupArn = 'arn:aws:iot:us-east-1:851725387994:thinggroup/thing-group'
thingArn = ''
thingId = ''
defaultPolicyName = 'things-policy'
###################################################

def createThing(thingName):
    global thingClient
    thingResponse = thingClient.create_thing(
        thingName = thingName
    )
    data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
    for element in data: 
        if element == 'thingArn':
            thingArn = data['thingArn']
        elif element == 'thingId':
            thingId = data['thingId']
    createCertificate(thingName)

def createCertificate(thingName):
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(
        setAsActive = True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data: 
        if element == 'certificateArn':
            certificateArn = data['certificateArn']
        elif element == 'keyPair':
            PublicKey = data['keyPair']['PublicKey']
            PrivateKey = data['keyPair']['PrivateKey']
        elif element == 'certificatePem':
            certificatePem = data['certificatePem']
        elif element == 'certificateId':
            certificateId = data['certificateId']

    public_key_dir = './certificates/device_{}/'.format(thingName)
    private_key_dir = './certificates/device_{}/'.format(thingName)
    cert_pem_dir = './certificates/device_{}/'.format(thingName)

    # Ensure directories exist
    os.makedirs(public_key_dir, exist_ok=True)
    os.makedirs(private_key_dir, exist_ok=True)
    os.makedirs(cert_pem_dir, exist_ok=True)

    public_key = './certificates/device_{}/device_{}.public.key'.format(thingName, thingName)
    private_key = './certificates/device_{}/device_{}.private.key'.format(thingName, thingName)
    cert_pem = './certificates/device_{}/device_{}.certificate.pem'.format(thingName, thingName)

    with open(public_key, 'w') as outfile:
        outfile.write(PublicKey)
    with open(private_key, 'w') as outfile:
        outfile.write(PrivateKey)
    with open(cert_pem, 'w') as outfile:
        outfile.write(certificatePem)

    response = thingClient.attach_policy(
        policyName = defaultPolicyName,
        target = certificateArn
    )
    response = thingClient.attach_thing_principal(
        thingName = thingName,
        principal = certificateArn
    )
    response = thingClient.add_thing_to_thing_group(
        thingGroupName=thingGroupName,
        thingGroupArn=thingGroupArn,
        thingName=thingName,
        thingArn=thingArn,
        overrideDynamicGroups=False
    )

thingClient = boto3.client('iot')

for i in range(5):
    num = "%d" % i
    createThing(num)