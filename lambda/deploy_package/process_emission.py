import logging
import platform
import sys
import json
from threading import Timer

import greengrasssdk

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

client = greengrasssdk.client("iot-data")

my_platform = platform.platform()

def calculate_max(event):
    logger.debug(event)
    response = {}
    if(int(event['current_max']) >= int(event['vehicle_CO2'])):
        response['new_max'] = int(event['current_max'])
    else:
        response['new_max'] = int(event['vehicle_CO2'])
    response['thing_id'] = event['thing_id']

    messageJson = json.dumps(response)
    client.publish(
        topic=event['thing_id'],
        queueFullPolicy="AllOrException",
        payload=messageJson,
    )

    response2 = {}
    response2['timestep_time'] = event['timestep_time']
    response2['vehicle_CO2'] = event['vehicle_CO2']
    response2['vehicle_HC'] = event['vehicle_HC']
    response2['vehicle_NOx'] = event['vehicle_NOx']
    response2['vehicle_PMx'] = event['vehicle_PMx']
    response2['vehicle_angle'] = event['vehicle_angle']
    response2['vehicle_eclass'] = event['vehicle_eclass']
    response2['vehicle_electricity'] = event['vehicle_electricity']
    response2['vehicle_fuel'] = event['vehicle_fuel']
    response2['vehicle_id'] = event['vehicle_id']
    response2['vehicle_lane'] = event['vehicle_lane']
    response2['vehicle_noise'] = event['vehicle_noise']
    response2['vehicle_pos'] = event['vehicle_pos']
    response2['vehicle_route'] = event['vehicle_route']
    response2['vehicle_speed'] = event['vehicle_speed']
    response2['vehicle_type'] = event['vehicle_type']
    response2['vehicle_waiting'] = event['vehicle_waiting']
    response2['vehicle_x'] = event['vehicle_x']
    response2['vehicle_y'] = event['vehicle_y']
    response2['sequence'] = event['sequence']
    response2['thing_id'] = event['thing_id']
    response2['isActive'] = event['isActive']
    response2['new_max'] = response['new_max']
    messageJson2 = json.dumps(response2)

    client.publish(
        topic="cardata",
        queueFullPolicy="AllOrException",
        payload=messageJson2,
    )
    return

def function_handler(event, context):
    calculate_max(event)
    return
