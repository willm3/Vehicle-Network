python3 basicDiscovery.py \
    --endpoint a23nokakp7wu3x-ats.iot.us-east-1.amazonaws.com \
    --rootCA certificates/root/AmazonRootCA1.pem \
    --cert certificates/device_4/device_4.certificate.pem \
    --key certificates/device_4/device_4.private.key \
    --thingName 4 \
    --topic '4' \
    --mode both \
    --message 'run'

python3 basicDiscovery.py \
    --endpoint a23nokakp7wu3x-ats.iot.us-east-1.amazonaws.com \
    --rootCA certificates/root/AmazonRootCA1.pem \
    --cert certificates/device_3/device_3.certificate.pem \
    --key certificates/device_3/device_3.private.key \
    --thingName 3 \
    --topic '3' \
    --mode both \
    --message 'run'

python3 basicDiscovery.py \
    --endpoint a23nokakp7wu3x-ats.iot.us-east-1.amazonaws.com \
    --rootCA certificates/root/AmazonRootCA1.pem \
    --cert certificates/device_2/device_2.certificate.pem \
    --key certificates/device_2/device_2.private.key \
    --thingName 2 \
    --topic '2' \
    --mode both \
    --message 'run'

python3 basicDiscovery.py \
    --endpoint a23nokakp7wu3x-ats.iot.us-east-1.amazonaws.com \
    --rootCA certificates/root/AmazonRootCA1.pem \
    --cert certificates/device_1/device_1.certificate.pem \
    --key certificates/device_1/device_1.private.key \
    --thingName 1 \
    --topic '1' \
    --mode both \
    --message 'run'

python3 basicDiscovery.py \
    --endpoint a23nokakp7wu3x-ats.iot.us-east-1.amazonaws.com \
    --rootCA certificates/root/AmazonRootCA1.pem \
    --cert certificates/device_0/device_0.certificate.pem \
    --key certificates/device_0/device_0.private.key \
    --thingName 0 \
    --topic '0' \
    --mode both \
    --message 'run'